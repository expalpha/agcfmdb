# adc_cuts
adc_bin_baseline_start  =   0
adc_bin_baseline_end    = 100
adc_bin_pc_middle_adc16 = 147
adc_bin_pc_middle_adc32 = 135
adc_bin_pulser_start    = 330
adc_bin_pulser_end      = 350
cut_baseline_rms = 500
cfd_thr_pct = 0.6
ph_hit_thr_adc16 = 1000
ph_hit_thr_adc32 = 1500
# end
